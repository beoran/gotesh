package gotesh

import "golang.org/x/text/language"
import "golang.org/x/image/font"
import "golang.org/x/image/font/sfnt"
import "image/color"
import "unicode"

type Language struct { 
    language.Tag
}

type Script struct { 
    language.Script
    Range []unicode.RangeTable
}

type LayoutFeatureType uint16 

const (
  LAYOUT_FEATURE_TYPE_INVALID				    = LayoutFeatureType(0xFFFF)
  LAYOUT_FEATURE_TYPE_ALL_TYPOGRAPHIC			= LayoutFeatureType(iota)
  LAYOUT_FEATURE_TYPE_LIGATURES 
  LAYOUT_FEATURE_TYPE_CURISVE_CONNECTION
  LAYOUT_FEATURE_TYPE_LETTER_CASE
  LAYOUT_FEATURE_TYPE_VERTICAL_SUBSTITUTION
  LAYOUT_FEATURE_TYPE_LINGUISTIC_REARRANGEMENT
  LAYOUT_FEATURE_TYPE_NUMBER_SPACING
  LAYOUT_FEATURE_TYPE_RESERVED_1		
  LAYOUT_FEATURE_TYPE_SMART_SWASH_TYPE			
  LAYOUT_FEATURE_TYPE_DIACRITICS_TYPE			
  LAYOUT_FEATURE_TYPE_VERTICAL_POSITION			
  LAYOUT_FEATURE_TYPE_FRACTIONS
  LAYOUT_FEATURE_TYPE_RESERVED_2
  LAYOUT_FEATURE_TYPE_OVERLAPPING_CHARACTERS_TYPE
  LAYOUT_FEATURE_TYPE_TYPOGRAPHIC_EXTRAS
  LAYOUT_FEATURE_TYPE_MATHEMATICAL_EXTRAS
  LAYOUT_FEATURE_TYPE_ORNAMENT_SETS_TYPE
  LAYOUT_FEATURE_TYPE_CHARACTER_ALTERNATIVES
  LAYOUT_FEATURE_TYPE_DESIGN_COMPLEXITY_TYPE
  LAYOUT_FEATURE_TYPE_STYLE_OPTIONS
  LAYOUT_FEATURE_TYPE_CHARACTER_SHAPE
  LAYOUT_FEATURE_TYPE_NUMBER_CASE
  LAYOUT_FEATURE_TYPE_TEXT_SPACING
  LAYOUT_FEATURE_TYPE_TRANSLITERATION
  LAYOUT_FEATURE_TYPE_ANNOTATION_TYPE
  LAYOUT_FEATURE_TYPE_KANA_SPACING_TYPE
  LAYOUT_FEATURE_TYPE_IDEOGRAPHIC_SPACING_TYPE
  LAYOUT_FEATURE_TYPE_UNICODE_DECOMPOSITION_TYPE
  LAYOUT_FEATURE_TYPE_RUBY_KANA
  LAYOUT_FEATURE_TYPE_CJK_SYMBOL_ALTERNATIVES_TYPE
  LAYOUT_FEATURE_TYPE_IDEOGRAPHIC_ALTERNATIVES_TYPE
  LAYOUT_FEATURE_TYPE_CJK_VERTICAL_ROMAN_PLACEMENT_TYPE
  LAYOUT_FEATURE_TYPE_ITALIC_CJK_ROMAN
  LAYOUT_FEATURE_TYPE_CASE_SENSITIVE_LAYOUT
  LAYOUT_FEATURE_TYPE_ALTERNATE_KANA
  LAYOUT_FEATURE_TYPE_STYLISTIC_ALTERNATIVES
  LAYOUT_FEATURE_TYPE_CONTEXTUAL_ALTERNATIVES
  LAYOUT_FEATURE_TYPE_LOWER_CASE
  LAYOUT_FEATURE_TYPE_UPPER_CASE
  LAYOUT_FEATURE_TYPE_LANGUAGE_TAG_TYPE
  LAYOUT_FEATURE_TYPE_CJK_ROMAN_SPACING_TYPE
  LAYOUT_FEATURE_TYPE_MAX_VALUE = LAYOUT_FEATURE_TYPE_CJK_ROMAN_SPACING_TYPE
)

type LayoutFeatureSelector uint16

const (
  LAYOUT_FEATURE_SELECTOR_INVALID			= LayoutFeatureSelector(0xFFFF)

  // Selectors for #LAYOUT_FEATURE_TYPE_ALL_TYPOGRAPHIC 
  LAYOUT_FEATURE_SELECTOR_ALL_TYPE_FEATURES_ON		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_ALL_TYPE_FEATURES_OFF		= LayoutFeatureSelector(1)

  // Selectors for #LAYOUT_FEATURE_TYPE_LIGATURES 
  LAYOUT_FEATURE_SELECTOR_REQUIRED_LIGATURES_ON		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_REQUIRED_LIGATURES_OFF		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_COMMON_LIGATURES_ON		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_COMMON_LIGATURES_OFF		= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_RARE_LIGATURES_ON		= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_RARE_LIGATURES_OFF		= LayoutFeatureSelector(5)
  LAYOUT_FEATURE_SELECTOR_LOGOS_ON			= LayoutFeatureSelector(6)
  LAYOUT_FEATURE_SELECTOR_LOGOS_OFF			= LayoutFeatureSelector(7)
  LAYOUT_FEATURE_SELECTOR_REBUS_PICTURES_ON		= LayoutFeatureSelector(8)
  LAYOUT_FEATURE_SELECTOR_REBUS_PICTURES_OFF		= LayoutFeatureSelector(9)
  LAYOUT_FEATURE_SELECTOR_DIPHTHONG_LIGATURES_ON		= LayoutFeatureSelector(10)
  LAYOUT_FEATURE_SELECTOR_DIPHTHONG_LIGATURES_OFF	= LayoutFeatureSelector(11)
  LAYOUT_FEATURE_SELECTOR_SQUARED_LIGATURES_ON		= LayoutFeatureSelector(12)
  LAYOUT_FEATURE_SELECTOR_SQUARED_LIGATURES_OFF		= LayoutFeatureSelector(13)
  LAYOUT_FEATURE_SELECTOR_ABBREV_SQUARED_LIGATURES_ON	= LayoutFeatureSelector(14)
  LAYOUT_FEATURE_SELECTOR_ABBREV_SQUARED_LIGATURES_OFF	= LayoutFeatureSelector(15)
  LAYOUT_FEATURE_SELECTOR_SYMBOL_LIGATURES_ON		= LayoutFeatureSelector(16)
  LAYOUT_FEATURE_SELECTOR_SYMBOL_LIGATURES_OFF		= LayoutFeatureSelector(17)
  LAYOUT_FEATURE_SELECTOR_CONTEXTUAL_LIGATURES_ON	= LayoutFeatureSelector(18)
  LAYOUT_FEATURE_SELECTOR_CONTEXTUAL_LIGATURES_OFF	= LayoutFeatureSelector(19)
  LAYOUT_FEATURE_SELECTOR_HISTORICAL_LIGATURES_ON	= LayoutFeatureSelector(20)
  LAYOUT_FEATURE_SELECTOR_HISTORICAL_LIGATURES_OFF	= LayoutFeatureSelector(21)

  // Selectors for #LAYOUT_FEATURE_TYPE_LIGATURES 
  LAYOUT_FEATURE_SELECTOR_UNCONNECTED			= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_PARTIALLY_CONNECTED		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_CURSIVE			= LayoutFeatureSelector(2)

  // Selectors for #LAYOUT_FEATURE_TYPE_LETTER_CASE 
  LAYOUT_FEATURE_SELECTOR_UPPER_AND_LOWER_CASE		= LayoutFeatureSelector(0) // deprecated 
  LAYOUT_FEATURE_SELECTOR_ALL_CAPS			= LayoutFeatureSelector(1) // deprecated 
  LAYOUT_FEATURE_SELECTOR_ALL_LOWER_CASE			= LayoutFeatureSelector(2) // deprecated 
  LAYOUT_FEATURE_SELECTOR_SMALL_CAPS			= LayoutFeatureSelector(3) // deprecated 
  LAYOUT_FEATURE_SELECTOR_INITIAL_CAPS			= LayoutFeatureSelector(4) // deprecated 
  LAYOUT_FEATURE_SELECTOR_INITIAL_CAPS_AND_SMALL_CAPS	= LayoutFeatureSelector(5) // deprecated 

  // Selectors for #LAYOUT_FEATURE_TYPE_VERTICAL_SUBSTITUTION 
  LAYOUT_FEATURE_SELECTOR_SUBSTITUTE_VERTICAL_FORMS_ON	= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_SUBSTITUTE_VERTICAL_FORMS_OFF	= LayoutFeatureSelector(1)

  // Selectors for #LAYOUT_FEATURE_TYPE_LINGUISTIC_REARRANGEMENT 
  LAYOUT_FEATURE_SELECTOR_LINGUISTIC_REARRANGEMENT_ON	= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_LINGUISTIC_REARRANGEMENT_OFF	= LayoutFeatureSelector(1)

  // Selectors for #LAYOUT_FEATURE_TYPE_NUMBER_SPACING 
  LAYOUT_FEATURE_SELECTOR_MONOSPACED_NUMBERS		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_PROPORTIONAL_NUMBERS		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_THIRD_WIDTH_NUMBERS		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_QUARTER_WIDTH_NUMBERS		= LayoutFeatureSelector(3)

  // Selectors for #LAYOUT_FEATURE_TYPE_SMART_SWASH_TYPE 
  LAYOUT_FEATURE_SELECTOR_WORD_INITIAL_SWASHES_ON	= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_WORD_INITIAL_SWASHES_OFF	= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_WORD_FINAL_SWASHES_ON		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_WORD_FINAL_SWASHES_OFF		= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_LINE_INITIAL_SWASHES_ON	= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_LINE_INITIAL_SWASHES_OFF	= LayoutFeatureSelector(5)
  LAYOUT_FEATURE_SELECTOR_LINE_FINAL_SWASHES_ON		= LayoutFeatureSelector(6)
  LAYOUT_FEATURE_SELECTOR_LINE_FINAL_SWASHES_OFF		= LayoutFeatureSelector(7)
  LAYOUT_FEATURE_SELECTOR_NON_FINAL_SWASHES_ON		= LayoutFeatureSelector(8)
  LAYOUT_FEATURE_SELECTOR_NON_FINAL_SWASHES_OFF		= LayoutFeatureSelector(9)

  // Selectors for #LAYOUT_FEATURE_TYPE_DIACRITICS_TYPE 
  LAYOUT_FEATURE_SELECTOR_SHOW_DIACRITICS		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_HIDE_DIACRITICS		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_DECOMPOSE_DIACRITICS		= LayoutFeatureSelector(2)

  // Selectors for #LAYOUT_FEATURE_TYPE_VERTICAL_POSITION 
  LAYOUT_FEATURE_SELECTOR_NORMAL_POSITION		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_SUPERIORS			= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_INFERIORS			= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_ORDINALS			= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_SCIENTIFIC_INFERIORS		= LayoutFeatureSelector(4)

  // Selectors for #LAYOUT_FEATURE_TYPE_FRACTIONS 
  LAYOUT_FEATURE_SELECTOR_NO_FRACTIONS			= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_VERTICAL_FRACTIONS		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_DIAGONAL_FRACTIONS		= LayoutFeatureSelector(2)

  // Selectors for #LAYOUT_FEATURE_TYPE_OVERLAPPING_CHARACTERS_TYPE 
  LAYOUT_FEATURE_SELECTOR_PREVENT_OVERLAP_ON		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_PREVENT_OVERLAP_OFF		= LayoutFeatureSelector(1)

  // Selectors for #LAYOUT_FEATURE_TYPE_TYPOGRAPHIC_EXTRAS 
  LAYOUT_FEATURE_SELECTOR_HYPHENS_TO_EM_DASH_ON		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_HYPHENS_TO_EM_DASH_OFF		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_HYPHEN_TO_EN_DASH_ON		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_HYPHEN_TO_EN_DASH_OFF		= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_SLASHED_ZERO_ON		= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_SLASHED_ZERO_OFF		= LayoutFeatureSelector(5)
  LAYOUT_FEATURE_SELECTOR_FORM_INTERROBANG_ON		= LayoutFeatureSelector(6)
  LAYOUT_FEATURE_SELECTOR_FORM_INTERROBANG_OFF		= LayoutFeatureSelector(7)
  LAYOUT_FEATURE_SELECTOR_SMART_QUOTES_ON		= LayoutFeatureSelector(8)
  LAYOUT_FEATURE_SELECTOR_SMART_QUOTES_OFF		= LayoutFeatureSelector(9)
  LAYOUT_FEATURE_SELECTOR_PERIODS_TO_ELLIPSIS_ON		= LayoutFeatureSelector(10)
  LAYOUT_FEATURE_SELECTOR_PERIODS_TO_ELLIPSIS_OFF	= LayoutFeatureSelector(11)

  // Selectors for #LAYOUT_FEATURE_TYPE_MATHEMATICAL_EXTRAS 
  LAYOUT_FEATURE_SELECTOR_HYPHEN_TO_MINUS_ON		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_HYPHEN_TO_MINUS_OFF		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_ASTERISK_TO_MULTIPLY_ON	= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_ASTERISK_TO_MULTIPLY_OFF	= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_SLASH_TO_DIVIDE_ON		= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_SLASH_TO_DIVIDE_OFF		= LayoutFeatureSelector(5)
  LAYOUT_FEATURE_SELECTOR_INEQUALITY_LIGATURES_ON	= LayoutFeatureSelector(6)
  LAYOUT_FEATURE_SELECTOR_INEQUALITY_LIGATURES_OFF	= LayoutFeatureSelector(7)
  LAYOUT_FEATURE_SELECTOR_EXPONENTS_ON			= LayoutFeatureSelector(8)
  LAYOUT_FEATURE_SELECTOR_EXPONENTS_OFF			= LayoutFeatureSelector(9)
  LAYOUT_FEATURE_SELECTOR_MATHEMATICAL_GREEK_ON		= LayoutFeatureSelector(10)
  LAYOUT_FEATURE_SELECTOR_MATHEMATICAL_GREEK_OFF		= LayoutFeatureSelector(11)

  // Selectors for #LAYOUT_FEATURE_TYPE_ORNAMENT_SETS_TYPE 
  LAYOUT_FEATURE_SELECTOR_NO_ORNAMENTS			= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_DINGBATS			= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_PI_CHARACTERS			= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_FLEURONS			= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_DECORATIVE_BORDERS		= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_INTERNATIONAL_SYMBOLS		= LayoutFeatureSelector(5)
  LAYOUT_FEATURE_SELECTOR_MATH_SYMBOLS			= LayoutFeatureSelector(6)

  // Selectors for #LAYOUT_FEATURE_TYPE_CHARACTER_ALTERNATIVES 
  LAYOUT_FEATURE_SELECTOR_NO_ALTERNATES			= LayoutFeatureSelector(0)

  // Selectors for #LAYOUT_FEATURE_TYPE_DESIGN_COMPLEXITY_TYPE 
  LAYOUT_FEATURE_SELECTOR_DESIGN_LEVEL1			= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_DESIGN_LEVEL2			= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_DESIGN_LEVEL3			= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_DESIGN_LEVEL4			= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_DESIGN_LEVEL5			= LayoutFeatureSelector(4)

  // Selectors for #LAYOUT_FEATURE_TYPE_STYLE_OPTIONS 
  LAYOUT_FEATURE_SELECTOR_NO_STYLE_OPTIONS		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_DISPLAY_TEXT			= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_ENGRAVED_TEXT			= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_ILLUMINATED_CAPS		= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_TITLING_CAPS			= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_TALL_CAPS			= LayoutFeatureSelector(5)

  // Selectors for #LAYOUT_FEATURE_TYPE_CHARACTER_SHAPE 
  LAYOUT_FEATURE_SELECTOR_TRADITIONAL_CHARACTERS		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_SIMPLIFIED_CHARACTERS		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_JIS1978_CHARACTERS		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_JIS1983_CHARACTERS		= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_JIS1990_CHARACTERS		= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_TRADITIONAL_ALT_ONE		= LayoutFeatureSelector(5)
  LAYOUT_FEATURE_SELECTOR_TRADITIONAL_ALT_TWO		= LayoutFeatureSelector(6)
  LAYOUT_FEATURE_SELECTOR_TRADITIONAL_ALT_THREE		= LayoutFeatureSelector(7)
  LAYOUT_FEATURE_SELECTOR_TRADITIONAL_ALT_FOUR		= LayoutFeatureSelector(8)
  LAYOUT_FEATURE_SELECTOR_TRADITIONAL_ALT_FIVE		= LayoutFeatureSelector(9)
  LAYOUT_FEATURE_SELECTOR_EXPERT_CHARACTERS		= LayoutFeatureSelector(10)
  LAYOUT_FEATURE_SELECTOR_JIS2004_CHARACTERS		= LayoutFeatureSelector(11)
  LAYOUT_FEATURE_SELECTOR_HOJO_CHARACTERS		= LayoutFeatureSelector(12)
  LAYOUT_FEATURE_SELECTOR_NLCCHARACTERS			= LayoutFeatureSelector(13)
  LAYOUT_FEATURE_SELECTOR_TRADITIONAL_NAMES_CHARACTERS	= LayoutFeatureSelector(14)

  // Selectors for #LAYOUT_FEATURE_TYPE_NUMBER_CASE 
  LAYOUT_FEATURE_SELECTOR_LOWER_CASE_NUMBERS		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_UPPER_CASE_NUMBERS		= LayoutFeatureSelector(1)

  // Selectors for #LAYOUT_FEATURE_TYPE_TEXT_SPACING 
  LAYOUT_FEATURE_SELECTOR_PROPORTIONAL_TEXT		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_MONOSPACED_TEXT		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_HALF_WIDTH_TEXT		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_THIRD_WIDTH_TEXT		= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_QUARTER_WIDTH_TEXT		= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_ALT_PROPORTIONAL_TEXT		= LayoutFeatureSelector(5)
  LAYOUT_FEATURE_SELECTOR_ALT_HALF_WIDTH_TEXT		= LayoutFeatureSelector(6)

  // Selectors for #LAYOUT_FEATURE_TYPE_TRANSLITERATION 
  LAYOUT_FEATURE_SELECTOR_NO_TRANSLITERATION		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_HANJA_TO_HANGUL		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_HIRAGANA_TO_KATAKANA		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_KATAKANA_TO_HIRAGANA		= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_KANA_TO_ROMANIZATION		= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_ROMANIZATION_TO_HIRAGANA	= LayoutFeatureSelector(5)
  LAYOUT_FEATURE_SELECTOR_ROMANIZATION_TO_KATAKANA	= LayoutFeatureSelector(6)
  LAYOUT_FEATURE_SELECTOR_HANJA_TO_HANGUL_ALT_ONE	= LayoutFeatureSelector(7)
  LAYOUT_FEATURE_SELECTOR_HANJA_TO_HANGUL_ALT_TWO	= LayoutFeatureSelector(8)
  LAYOUT_FEATURE_SELECTOR_HANJA_TO_HANGUL_ALT_THREE	= LayoutFeatureSelector(9)

  // Selectors for #LAYOUT_FEATURE_TYPE_ANNOTATION_TYPE 
  LAYOUT_FEATURE_SELECTOR_NO_ANNOTATION			= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_BOX_ANNOTATION			= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_ROUNDED_BOX_ANNOTATION		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_CIRCLE_ANNOTATION		= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_INVERTED_CIRCLE_ANNOTATION	= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_PARENTHESIS_ANNOTATION		= LayoutFeatureSelector(5)
  LAYOUT_FEATURE_SELECTOR_PERIOD_ANNOTATION		= LayoutFeatureSelector(6)
  LAYOUT_FEATURE_SELECTOR_ROMAN_NUMERAL_ANNOTATION	= LayoutFeatureSelector(7)
  LAYOUT_FEATURE_SELECTOR_DIAMOND_ANNOTATION		= LayoutFeatureSelector(8)
  LAYOUT_FEATURE_SELECTOR_INVERTED_BOX_ANNOTATION	= LayoutFeatureSelector(9)
  LAYOUT_FEATURE_SELECTOR_INVERTED_ROUNDED_BOX_ANNOTATION= LayoutFeatureSelector(10)

  // Selectors for #LAYOUT_FEATURE_TYPE_KANA_SPACING_TYPE 
  LAYOUT_FEATURE_SELECTOR_FULL_WIDTH_KANA		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_PROPORTIONAL_KANA		= LayoutFeatureSelector(1)

  // Selectors for #LAYOUT_FEATURE_TYPE_IDEOGRAPHIC_SPACING_TYPE 
  LAYOUT_FEATURE_SELECTOR_FULL_WIDTH_IDEOGRAPHS		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_PROPORTIONAL_IDEOGRAPHS	= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_HALF_WIDTH_IDEOGRAPHS		= LayoutFeatureSelector(2)

  // Selectors for #LAYOUT_FEATURE_TYPE_UNICODE_DECOMPOSITION_TYPE 
  LAYOUT_FEATURE_SELECTOR_CANONICAL_COMPOSITION_ON	= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_CANONICAL_COMPOSITION_OFF	= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_COMPATIBILITY_COMPOSITION_ON	= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_COMPATIBILITY_COMPOSITION_OFF	= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_TRANSCODING_COMPOSITION_ON	= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_TRANSCODING_COMPOSITION_OFF	= LayoutFeatureSelector(5)

  // Selectors for #LAYOUT_FEATURE_TYPE_RUBY_KANA 
  LAYOUT_FEATURE_SELECTOR_NO_RUBY_KANA			= LayoutFeatureSelector(0) // deprecated - use LAYOUT_FEATURE_SELECTOR_RUBY_KANA_OFF instead 
  LAYOUT_FEATURE_SELECTOR_RUBY_KANA			= LayoutFeatureSelector(1) // deprecated - use LAYOUT_FEATURE_SELECTOR_RUBY_KANA_ON instead 
  LAYOUT_FEATURE_SELECTOR_RUBY_KANA_ON			= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_RUBY_KANA_OFF			= LayoutFeatureSelector(3)

  // Selectors for #LAYOUT_FEATURE_TYPE_CJK_SYMBOL_ALTERNATIVES_TYPE 
  LAYOUT_FEATURE_SELECTOR_NO_CJK_SYMBOL_ALTERNATIVES	= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_CJK_SYMBOL_ALT_ONE		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_CJK_SYMBOL_ALT_TWO		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_CJK_SYMBOL_ALT_THREE		= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_CJK_SYMBOL_ALT_FOUR		= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_CJK_SYMBOL_ALT_FIVE		= LayoutFeatureSelector(5)

  // Selectors for #LAYOUT_FEATURE_TYPE_IDEOGRAPHIC_ALTERNATIVES_TYPE 
  LAYOUT_FEATURE_SELECTOR_NO_IDEOGRAPHIC_ALTERNATIVES	= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_IDEOGRAPHIC_ALT_ONE		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_IDEOGRAPHIC_ALT_TWO		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_IDEOGRAPHIC_ALT_THREE		= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_IDEOGRAPHIC_ALT_FOUR		= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_IDEOGRAPHIC_ALT_FIVE		= LayoutFeatureSelector(5)

  // Selectors for #LAYOUT_FEATURE_TYPE_CJK_VERTICAL_ROMAN_PLACEMENT_TYPE 
  LAYOUT_FEATURE_SELECTOR_CJK_VERTICAL_ROMAN_CENTERED	= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_CJK_VERTICAL_ROMAN_HBASELINE	= LayoutFeatureSelector(1)

  // Selectors for #LAYOUT_FEATURE_TYPE_ITALIC_CJK_ROMAN 
  LAYOUT_FEATURE_SELECTOR_NO_CJK_ITALIC_ROMAN		= LayoutFeatureSelector(0)    // deprecated - use LAYOUT_FEATURE_SELECTOR_CJK_ITALIC_ROMAN_OFF instead 
  LAYOUT_FEATURE_SELECTOR_CJK_ITALIC_ROMAN		= LayoutFeatureSelector(1)    // deprecated - use LAYOUT_FEATURE_SELECTOR_CJK_ITALIC_ROMAN_ON instead 
  LAYOUT_FEATURE_SELECTOR_CJK_ITALIC_ROMAN_ON		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_CJK_ITALIC_ROMAN_OFF		= LayoutFeatureSelector(3)

  // Selectors for #LAYOUT_FEATURE_TYPE_CASE_SENSITIVE_LAYOUT 
  LAYOUT_FEATURE_SELECTOR_CASE_SENSITIVE_LAYOUT_ON	= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_CASE_SENSITIVE_LAYOUT_OFF	= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_CASE_SENSITIVE_SPACING_ON	= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_CASE_SENSITIVE_SPACING_OFF	= LayoutFeatureSelector(3)

  // Selectors for #LAYOUT_FEATURE_TYPE_ALTERNATE_KANA 
  LAYOUT_FEATURE_SELECTOR_ALTERNATE_HORIZ_KANA_ON	= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_ALTERNATE_HORIZ_KANA_OFF	= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_ALTERNATE_VERT_KANA_ON		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_ALTERNATE_VERT_KANA_OFF	= LayoutFeatureSelector(3)

  // Selectors for #LAYOUT_FEATURE_TYPE_STYLISTIC_ALTERNATIVES 
  LAYOUT_FEATURE_SELECTOR_NO_STYLISTIC_ALTERNATES	= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_ONE_ON		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_ONE_OFF		= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_TWO_ON		= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_TWO_OFF		= LayoutFeatureSelector(5)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_THREE_ON		= LayoutFeatureSelector(6)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_THREE_OFF	= LayoutFeatureSelector(7)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_FOUR_ON		= LayoutFeatureSelector(8)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_FOUR_OFF		= LayoutFeatureSelector(9)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_FIVE_ON		= LayoutFeatureSelector(10)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_FIVE_OFF		= LayoutFeatureSelector(11)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_SIX_ON		= LayoutFeatureSelector(12)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_SIX_OFF		= LayoutFeatureSelector(13)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_SEVEN_ON		= LayoutFeatureSelector(14)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_SEVEN_OFF	= LayoutFeatureSelector(15)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_EIGHT_ON		= LayoutFeatureSelector(16)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_EIGHT_OFF	= LayoutFeatureSelector(17)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_NINE_ON		= LayoutFeatureSelector(18)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_NINE_OFF		= LayoutFeatureSelector(19)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_TEN_ON		= LayoutFeatureSelector(20)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_TEN_OFF		= LayoutFeatureSelector(21)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_ELEVEN_ON	= LayoutFeatureSelector(22)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_ELEVEN_OFF	= LayoutFeatureSelector(23)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_TWELVE_ON	= LayoutFeatureSelector(24)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_TWELVE_OFF	= LayoutFeatureSelector(25)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_THIRTEEN_ON	= LayoutFeatureSelector(26)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_THIRTEEN_OFF	= LayoutFeatureSelector(27)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_FOURTEEN_ON	= LayoutFeatureSelector(28)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_FOURTEEN_OFF	= LayoutFeatureSelector(29)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_FIFTEEN_ON	= LayoutFeatureSelector(30)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_FIFTEEN_OFF	= LayoutFeatureSelector(31)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_SIXTEEN_ON	= LayoutFeatureSelector(32)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_SIXTEEN_OFF	= LayoutFeatureSelector(33)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_SEVENTEEN_ON	= LayoutFeatureSelector(34)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_SEVENTEEN_OFF	= LayoutFeatureSelector(35)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_EIGHTEEN_ON	= LayoutFeatureSelector(36)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_EIGHTEEN_OFF	= LayoutFeatureSelector(37)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_NINETEEN_ON	= LayoutFeatureSelector(38)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_NINETEEN_OFF	= LayoutFeatureSelector(39)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_TWENTY_ON	= LayoutFeatureSelector(40)
  LAYOUT_FEATURE_SELECTOR_STYLISTIC_ALT_TWENTY_OFF	= LayoutFeatureSelector(41)

  // Selectors for #LAYOUT_FEATURE_TYPE_CONTEXTUAL_ALTERNATIVES 
  LAYOUT_FEATURE_SELECTOR_CONTEXTUAL_ALTERNATES_ON	= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_CONTEXTUAL_ALTERNATES_OFF	= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_SWASH_ALTERNATES_ON		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_SWASH_ALTERNATES_OFF		= LayoutFeatureSelector(3)
  LAYOUT_FEATURE_SELECTOR_CONTEXTUAL_SWASH_ALTERNATES_ON	= LayoutFeatureSelector(4)
  LAYOUT_FEATURE_SELECTOR_CONTEXTUAL_SWASH_ALTERNATES_OFF= LayoutFeatureSelector(5)

  // Selectors for #LAYOUT_FEATURE_TYPE_LOWER_CASE 
  LAYOUT_FEATURE_SELECTOR_DEFAULT_LOWER_CASE		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_LOWER_CASE_SMALL_CAPS		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_LOWER_CASE_PETITE_CAPS		= LayoutFeatureSelector(2)

  // Selectors for #LAYOUT_FEATURE_TYPE_UPPER_CASE 
  LAYOUT_FEATURE_SELECTOR_DEFAULT_UPPER_CASE		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_UPPER_CASE_SMALL_CAPS		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_UPPER_CASE_PETITE_CAPS		= LayoutFeatureSelector(2)

  // Selectors for #LAYOUT_FEATURE_TYPE_CJK_ROMAN_SPACING_TYPE 
  LAYOUT_FEATURE_SELECTOR_HALF_WIDTH_CJK_ROMAN		= LayoutFeatureSelector(0)
  LAYOUT_FEATURE_SELECTOR_PROPORTIONAL_CJK_ROMAN		= LayoutFeatureSelector(1)
  LAYOUT_FEATURE_SELECTOR_DEFAULT_CJK_ROMAN		= LayoutFeatureSelector(2)
  LAYOUT_FEATURE_SELECTOR_FULL_WIDTH_CJK_ROMAN		= LayoutFeatureSelector(3)

)

type Face struct {
    font.Face
}

func (face *Face) LayoutFeatureTypes() []LayoutFeatureType {
    return nil
}

func (lft LayoutFeatureType) Name() string {
    return ""
}

type LayoutFeatureSelectorInfo struct { 
    Name string
    Enable LayoutFeatureSelector
    Disable LayoutFeatureSelector
    reserved uint
}

const LAYOUT_NO_SELECTOR_INDEX = 0xFFFF

func (face *Face) SelectorInfos() []LayoutFeatureSelectorInfo {
    return nil
}

func (face *Face) LayoutHasSubstitution() bool {
    return false
}

func (face * Face) LayoutHasPositioning() bool {
    return false
}

func (face * Face) LayoutHasTracking() bool {
    return false
}

/* HarfBuzz had a whole "blob" based memory system which is not needed in Go. */



/* GlyphInfo contains information about the glyphs and their relation 
 * to input text.
 * @codepoint: either a Unicode code point (before shaping) or a glyph index
 *             (after shaping).
 * @cluster: the index of the character in the original text that corresponds
 *           to this GlyphInfo, or whatever the client passes to
 *           hb_buffer_add(). More than one #GlyphInfo can have the same
 *           @cluster value, if they resulted from the same character (e.g. one
 *           to many glyph substitution), and when more than one character gets
 *           merged in the same glyph (e.g. many to one glyph substitution) the
 *           #GlyphInfo will have the smallest cluster value of them.
 *           By default some characters are merged into the same cluster
 *           (e.g. combining marks have the same cluster as their bases)
 *           even if they are separate glyphs, hb_buffer_set_cluster_level()
 *           allow selecting more fine-grained cluster handling.
 */
type GlyphInfo struct {
    Codepoint rune
    Cluster uint32
    mask uint64
    var1 int64
    var2 int64
}

/* GLYPH_FLAG_UNSAFE_TO_BREAK: Indicates that if input text is broken at the
 * 				   beginning of the cluster this glyph is part of,
 * 				   then both sides need to be re-shaped, as the
 * 				   result might be different.  On the flip side,
 * 				   it means that when this flag is not present,
 * 				   then it's safe to break the glyph-run at the
 * 				   beginning of this cluster, and the two sides
 * 				   represent the exact same result one would get
 * 				   if breaking input text at the beginning of
 * 				   this cluster and shaping the two sides
 * 				   separately.  This can be used to optimize
 * 				   paragraph layout, by avoiding re-shaping
 * 				   of each line after line-breaking, or limiting
 * 				   the reshaping to a small piece around the
 * 				   breaking point only.
 */
const  GLYPH_FLAG_UNSAFE_TO_BREAK = 1
const  GLYPH_FLAG_DEFINED		  = 1

func (info *GlyphInfo) SetFlag(flag uint64) {
    info.mask |= flag 
}

func (info *GlyphInfo) IsFlag(flag uint64) bool {
    return (info.mask | flag) == flag 
}

func (info *GlyphInfo) UnsafeToBreak() bool {
    return info.IsFlag(GLYPH_FLAG_UNSAFE_TO_BREAK)
}


/**
 * GlyphPosition:
 * XAdvance: how much the line advances after drawing this glyph when setting
 *             text in horizontal direction.
 * YAdvance: how much the line advances after drawing this glyph when setting
 *             text in vertical direction.
 * XOffset: how much the glyph moves on the X-axis before drawing it, this
 *            should not affect how much the line advances.
 * YOffset: how much the glyph moves on the Y-axis before drawing it, this
 *            should not affect how much the line advances.
 *
 * GlyphPosition holds the positions of the
 * glyph in both horizontal and vertical directions. All positions in
 * 
 * GlyphPosition are relative to the current point.
 *
 */
type GlyphPosition struct {
    XAdvance Position;
    YAdvance Position;
    XOffset Position;
    YOffset Position;
    var1 int64
}

/**
 * SegmentProperties:
 * @direction: the #hb_direction_t of the buffer, see hb_buffer_set_direction().
 * @script: the #hb_script_t of the buffer, see hb_buffer_set_script().
 * @language: the #hb_language_t of the buffer, see hb_buffer_set_language().
 * SegmentProperties holds various text properties of a Buffer. Can be
 * set and retrieved using SetSegmentProperties() and
 * GetSegmentProperties(), respectively.
 */
type SegmentProperties struct {
  Direction
  Script
  Language
}

/* Segment is a segment of text with uniform language, direction and script 
 *
 */
type Segment struct {
    SegmentProperties
    Begin int64
    End int64
}

/**
 * Buffer holds the input text and its properties before shaping.
 */
type Buffer struct {
    Input string
    SegmentProperties // properties for the whole buffer by default
    BufferFlags
    ClusterLevel
    Segments []Segment
}


// Tries to split the buffer in segments and guess their properties,
// based on the text in the buffer.
func (bufer *Buffer) GuessSegmentProperties() error {
    // XXX todo
    return nil
}

type BufferFlags uint

const (
    BUFFER_FLAG_DEFAULT			= BufferFlags(0)
    BUFFER_FLAG_BOT				= BufferFlags(1) /* Beginning-of-text */
    BUFFER_FLAG_EOT				= BufferFlags(2) /* End-of-text */
    BUFFER_FLAG_PRESERVE_DEFAULT_IGNORABLES	= BufferFlags(4)
    BUFFER_FLAG_REMOVE_DEFAULT_IGNORABLES	= BufferFlags(8)
    BUFFER_FLAG_DO_NOT_INSERT_DOTTED_CIRCLE	= BufferFlags(16)
)

func (buffer *Buffer) SetFlag(flag BufferFlags) {
    buffer.BufferFlags |= flag
}

func (buffer Buffer) IsFlag(flag BufferFlags) bool {
    return (buffer.BufferFlags | flag) == flag 
}

type ClusterLevel int

const (
  BUFFER_CLUSTER_LEVEL_MONOTONE_GRAPHEMES	= ClusterLevel(iota)
  BUFFER_CLUSTER_LEVEL_MONOTONE_CHARACTERS
  BUFFER_CLUSTER_LEVEL_CHARACTERS
)

/**
 * BUFFER_REPLACEMENT_CODEPOINT_DEFAULT:
 *
 * The default code point for replacing invalid characters in a given encoding.
 * Set to U+FFFD REPLACEMENT CHARACTER.
 *
 * Since: 0.9.31
 */
const BUFFER_REPLACEMENT_CODEPOINT_DEFAULT = rune(0xFFFD)

func (buffer *Buffer) Reset() {
    // XXX Todo.
}

// Dropped buffer reversing for now.

/* Filling the buffer in is easy in Go, no need for 7 functions 
 * for that for now. */
 
// no need for normalization either, that is available in x/text/unicode/norm

// Dropped serialization for now.

// Unlike in HarfBuzz, tags are simply strings, which they should be, in stead 
// of a 4 character string squeezed into a uint32.
type Tag string

const TAG_NONE = ""


type Position int32
type Mask uint32

type Direction uint8

const (
  DIRECTION_LTR = Direction(iota)
  DIRECTION_RTL
  DIRECTION_TTB
  DIRECTION_BTT
  DIRECTION_UNKNOWN
)

func DirectionFromString(s string) Direction {
    return DIRECTION_UNKNOWN
}

func (d Direction) IsHorizontal() bool {
        return d == DIRECTION_LTR || d == DIRECTION_RTL
}

func (d Direction) IsVertical() bool {
        return d == DIRECTION_TTB || d == DIRECTION_BTT
}

// Language and script are defined in other go libraries, see above. 

/* Buffer features and variations. */

const FEATURE_GLOBAL_START = 0
const FEATURE_GLOBAL_END = -1

/* Feature is the structure that holds information about requested
 * feature application. The feature will be applied with the given value to all
 * glyphs which are in clusters between start (inclusive) and end (exclusive).
 * Setting start to FEATURE_GLOBAL_START and end to FEATURE_GLOBAL_END
 * specifies that the feature always applies to the entire buffer.
 */
type Feature struct {
  Tag
  Value uint32
  Start int
  End int
}


type Variation struct {
  Tag
  Value float32
}


/* Color is the data type for type for holding color values.
 * It is simply an alias for image/color.RGBA
 */
type Color = color.RGBA

// Creates a new color. Note that the creation order is b g r a because 
// that is what HarfBuzz uses.
func NewColor(b,g,r,a uint8) Color { 
    return Color { r, g, b, a } 
}

// HarfBuzz has support for CoreText and DirectWrite which we don't.

// Interface for user supplied drawing. The drawing will need to keep the state
// of the last point drawn to by itself.
type Drawer interface { 
    // Moves the pen of the drawer to the given position without drawing
    MoveTo(toX, toY Position)
    // Draws a straight line to the target points, moving the pen.
    LineTo(toX, toY Position)
    // Draws a quadratic line to the target points, moving the pen.
    QuadraticTo(controlX, controlY, toX, toY Position)
    // Draws a cubic line to the target points, moving the pen.
    CubicTo(control1X, control1Y, control2X, control2Y, toX, toY Position)
    // Closes the path. The path begins at the first time the drawer is called,
    // and at any call to colosepath. The closed path may be filled.
    ClosePath()
}

// A font, based on golang.org/x/image/sfnt.Font
type Font struct { 
     sfnt.Font
}

